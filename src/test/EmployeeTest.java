package test;

import domein.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeTest {
    Engineer e1;
    Engineer e2;

    Salesman s1;
    Salesman s2;

    Manager m1;
    Manager m2;

    final double COMMISSIE = 100;
    final double BONUS = 200;
    final double DELTA = 1E-15;


    @Before
    public void setUp() {
        e1 = new Engineer(1,1000);
        e2 = new Engineer(2,2000);

        s1 = new Salesman(3,1000, COMMISSIE);
        s2 = new Salesman(4,2000, COMMISSIE);

        m1 = new Manager(5,1000, BONUS);
        m2 = new Manager(303120913,4000, BONUS);
    }

    /**
     * Geeft het ID terug van het object.
     */
    @Test
    public void getIdTest() {
        assertEquals(1, e1.getId());
        assertEquals(2, e2.getId());
        assertEquals(3, s1.getId());
        assertEquals(4, s2.getId());
        assertEquals(5, m1.getId());
        assertEquals(303120913, m2.getId());
    }

    /**
     * De maandsalarissen worden berekent, waarbij een DELTA is meegegegeven zodat
     * het verschil met vijftien cijfers achter de komma mag voorkomen.
     * De methode houdt ook rekening mee met de eventuele commissies en bonussen
     * die erbij komen.
     */
    @Test
    public void payAmountTest() {
        assertEquals(1000, e1.payAmount(), DELTA);
        assertEquals(2100, s2.payAmount(), DELTA);
        assertEquals(4200, m2.payAmount(), DELTA);
    }

    /**
     * De manager van de engineer en salesman worden vergeleken met de manager
     * die ook daadwerkelijk is toegewezen.
     */
    @Test
    public void getManagerTest() {
        e1.setManager(m1);
        s1.setManager(m2);

        assertEquals(m1, e1.getManager());
        assertEquals(m2, s1.getManager());
    }

    /**
     * Bij een subtype employee wordt een poging gedaan om de manager te krijgen, maar
     * de manager is nooit toegevoegd. Er wordt dus een null teruggegeven.
     */
    @Test
    public void getManagerNullTest() {
        assertNull(e1.getManager());
        assertNull(e2.getManager());
        assertNull(s1.getManager());
        assertNull(s2.getManager());
        assertNull(m1.getManager());
        assertNull(m2.getManager());
    }


    /**
     * Object in de parameter is een manager en kan dus toegewezen worden aan elk type
     * employee. Daarbij kan de manager een manager zijn van meerdere engineers, salesmannen
     * en/of managers.
     */
    @Test
    public void setManagerTest() {
        assertTrue(e1.setManager(m1));
        assertTrue(e2.setManager(m2));
        assertTrue(s1.setManager(m1));
        assertTrue(s2.setManager(m2));
        assertTrue(m1.setManager(m1));
        assertTrue(m2.setManager(m2));
    }

    /**
     * Hierbij betreft het geen manager en kan dus niet toegewezen worden aan een object.
     */
    @Test
    public void setManagerIsGeenManagerTest() {
        assertFalse(e1.setManager(e2));
        assertFalse(e2.setManager(s1));
        assertFalse(s1.setManager(e2));
        assertFalse(m1.setManager(e1));
    }

    /**
     * Equals methode kijkt of de ID's van twee objecten gelijk zijn.
     *
     * De objecten in deze test hebben dezelfde id's als de objecten die
     * in de @before zijn aangemaakt
     */
    @Test
    public void equalsTest() {
        Engineer testEngineer = new Engineer(1, 1000);
        Salesman testSalesman = new Salesman(3, 1000, 100);
        assertTrue(e1.equals(testEngineer));
        assertTrue(s1.equals(testSalesman));

        /*
        Vergelijking met twee verschillende aangemaakte objecten, maar wel met dezelfde id.
        Levert dus ook een true op.
         */
        Manager manager4 = new Manager(3, 1000, 100);
        Engineer engineer5 = new Engineer(3, 1000);
        assertTrue(manager4.equals(engineer5));
    }

    /**
     * Het aangemaakte Engineer object met ID = 15 bestaat nog niet. De equals methode geeft dus een false terug,
     * omdat geen van de al toegevoegde objecten in de lijst een ID 15 hebben.
     */
    @Test
    public void equalsTestMetUniekeID() {
        Engineer testEngineer2 = new Engineer(15, 1000);
        assertFalse(e1.equals(testEngineer2));
        assertFalse(e2.equals(testEngineer2));
        assertFalse(s1.equals(testEngineer2));
        assertFalse(s2.equals(testEngineer2));
        assertFalse(m1.equals(testEngineer2));
        assertFalse(m2.equals(testEngineer2));
    }

    /**
     * De omschrijvingen van de objecten worden teruggeven.
     */
    @Test
    public void toStringTest() {
        s1.setManager(m2);
        assertEquals("Type: Engineer, id: 1, monthly salary: 1000.0." + "\n", e1.toString());
        assertEquals("Type: Salesman, id: 3, monthly salary: 1000.0, commission: 100.0, manager: 303120913." + "\n", s1.toString());
        assertEquals("Type: Manager, id: 5, monthly salary: 1000.0, bonus: 200.0." + "\n", m1.toString());
    }
}