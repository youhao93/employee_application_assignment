package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;

import domein.EmployeesAdmin;

/**
 * JUnit test voor klasse EmployeesAdmin
 */
public class EmployeesAdminTest {
    final double COMMISSIE = 100;
    final double BONUS = 200;
    final double DELTA = 1E-15;

    EmployeesAdmin employeesAdmin;

    @Before
    public void setUp() {
        employeesAdmin = new EmployeesAdmin();
        employeesAdmin.addEngineer(1, 1000);
        employeesAdmin.addEngineer(2, 2000);

        employeesAdmin.addSalesman(3, 1000, COMMISSIE);
        employeesAdmin.addSalesman(4, 2000, COMMISSIE);

        employeesAdmin.addManager(5, 1000, BONUS);
        employeesAdmin.addManager(303120913, 4000, BONUS);
    }


    /**
     * Scenario waarbij het Engineer-object dat toegevoegd moet worden, al een object
     * met zelfde id in de lijst is. De add methode moet dus een false teruggeven.
     */
    @Test
    public void addEngineerTest() {
        assertFalse(employeesAdmin.addEngineer(1, 1000));
        assertFalse(employeesAdmin.addEngineer(2, 1000));
    }

    /**
     * Scenario waarbij het Salesman-object dat toegevoegd moet worden, al een object
     * met zelfde id in de lijst is. De add methode moet dus een false teruggeven.
     */
    @Test
    public void addSalesmanTest() {
        assertFalse(employeesAdmin.addSalesman(4, 1929, COMMISSIE));
        assertFalse(employeesAdmin.addSalesman(5, 123, COMMISSIE));
        assertFalse(employeesAdmin.addSalesman(303120913, 2038, COMMISSIE));
    }

    /**
     * Scenario waarbij het Manager-object dat toegevoegd moet worden, al een object
     * met zelfde id in de lijst is. De add methode moet dus een false teruggeven.
     */
    @Test
    public void addManagerTest() {
        assertFalse(employeesAdmin.addManager(3, 1929, BONUS));
        assertFalse(employeesAdmin.addManager(5, 1929, BONUS));
    }

    /**
     * Scenario waarbij het nieuwe Engineer-object nog niet eerder voorkomt in de
     * lijst met dezelfde ID. Kan dus toegevoegd worden en geeft een true terug.
     */
    @Test
    public void addEngineerUniekeIDTest() {
        assertTrue(employeesAdmin.addEngineer(6, 1000));
        assertTrue(employeesAdmin.addEngineer(7, 1000));
        assertTrue(employeesAdmin.addManager(8, 1929, BONUS));
    }

    /**
     * Scenario waarbij het nieuwe Salesman-object nog niet eerder voorkomt in de
     * lijst met dezelfde ID. Kan dus toegevoegd worden en geeft een true terug.
     */
    @Test
    public void addSalesmanUniekeIDTest() {
        assertTrue(employeesAdmin.addSalesman(6, 1000, COMMISSIE));
        assertTrue(employeesAdmin.addSalesman(7, 1000, COMMISSIE));
        assertTrue(employeesAdmin.addSalesman(8, 1929, COMMISSIE));
    }

    /**
     * Scenario waarbij het nieuwe Manager-object nog niet eerder voorkomt in de
     * lijst met dezelfde ID. Kan dus toegevoegd worden en geeft een true terug.
     */
    @Test
    public void addManagerUniekeIDTest() {
        assertTrue(employeesAdmin.addManager(6, 1000, COMMISSIE));
        assertTrue(employeesAdmin.addManager(7, 1000, COMMISSIE));
        assertTrue(employeesAdmin.addManager(8, 1929, COMMISSIE));
    }

    /**
     * Berekent de totale waarde van het salaris, waarbij het verschil met vijftien
     * cijfers achter de komma mag voorkomen.
     */
    @Test
    public void calcTotalPayAmountTest() {
        assertEquals(11600, employeesAdmin.calcTotalPayAmount(), DELTA);
    }

    /**
     * Test of de methode toString de juiste omschrijvingen krijgen
     */
    @Test
    public void toStringTest() {
        assertEquals("Type: Engineer, id: 1, monthly salary: 1000.0." + "\n" +
                "Type: Engineer, id: 2, monthly salary: 2000.0." + "\n" +
                "Type: Salesman, id: 3, monthly salary: 1000.0, commission: 100.0." + "\n" +
                "Type: Salesman, id: 4, monthly salary: 2000.0, commission: 100.0." + "\n" +
                "Type: Manager, id: 5, monthly salary: 1000.0, bonus: 200.0." + "\n" +
                "Type: Manager, id: 303120913, monthly salary: 4000.0, bonus: 200.0." + "\n",
                employeesAdmin.toString());
    }

    @Test
    public void getHighestIdTest() {
        assertEquals(303120913, employeesAdmin.getHighestId());
    }

    /**
     * Scenario waarbij de aangewezen managers toegewezen kunnen worden aan een
     * Engineer, salesman of manager.
     */
    @Test
    public void setManagerIsAManagerTest() {
        assertTrue(employeesAdmin.setManager(0, 4));
        assertTrue(employeesAdmin.setManager(2, 5));
        assertTrue(employeesAdmin.setManager(3, 5));
    }

    /**
     * Scenario waarbij de aangewezen engineer of salesman NIET toegewezen kunnen worden aan een
     * Engineer, salesman of manager.
     */
    @Test
    public void setManagerIsNotAManagerTest() {
        assertFalse(employeesAdmin.setManager(1, 2));
        assertFalse(employeesAdmin.setManager(2, 1));
        assertFalse(employeesAdmin.setManager(3, 2));
    }
}