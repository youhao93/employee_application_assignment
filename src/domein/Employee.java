package domein;

/**
 * Abstracte klasse van Employee die zelf geen instantie kan krijgen.
 * Het heeft een aantal geïmplementeerde als abstracte methoden. In
 * deze abstracte klasse wordt het id, maandsalaris en toegewezen manager
 * opgeslagen die vervolgens opgehaald of veranderd kunnen worden met de
 * get- en set-methoden. De abstracte methoden worden door de subklasses van
 * Employee geïmplementeerd.
 */
abstract class Employee {
	
	private int id = 0;
	private double monthlySalary = 0.0;
	private Employee manager = null;

	/**
	 * Maakt een nieuw Employee object aan met het gegeven id nummer en
	 * maandsalaris.
	 *
	 * @param id het id nummer dat het Employee object krijgt
	 * @param monthlySalary het maandsalaris dat het Employee object krijgt
	 */
	public Employee(int id, double monthlySalary) {
		this.id = id;
		this.monthlySalary = monthlySalary;
	}

	/**
	 * Methode getId geeft het id nummer terug.
	 *
	 * @return id nummer
	 */
	public int getId() {
		return id;
	}

	/**
	 * Geeft het totale maandsalaris terug. Dit is afhankelijk van
	 * het actuele type object, waarbij eventueel een bonus of commissie erbij
	 * wordt opgeteld.
	 *
	 * @return totale maandsalaris
	 */
	public double payAmount() {
		return monthlySalary;
	}

	/**
	 * Methode getMonthlySalaray geeft alleen het maandsalaris terug.
	 *
	 * @return maandsalaris
	 */
	public double getMonthlySalary() {
		return monthlySalary;
	}

	@Override
	/**
	 * Methode equals controleert of twee id's hetzelfde zijn.
	 *
	 * @param e Employee object
	 * @return true als twee id's hetzelfde getal zijn, anders false
	 */
	public boolean equals(Object e) {
		int id2 = ((Employee)e).getId();
		return this.id == id2;
	}

	/**
	 * Methode getManager geeft het Manager object terug.
	 *
	 * @return Manager object
	 */
	public Employee getManager() {
		return manager;
	}


	/**
	 * Methode setManager wijst een Manager-object aan een Employee-object.
	 * Er wordt nog eerst gecontroleerd of het object in de parameter een
	 * instantie is van Manager. Als het een true is, dan wordt de manager
	 * toegewezen aan het object.
	 *
	 * @param manager die wordt toegewezen aan een Employee-object
	 * @return true als meegegeven object in parameter een instantie is van Manager
	 * en is toegewezen aan Employee-object, anders wordt een false teruggegeven
	 */
	public boolean setManager(Employee manager) {
		if (manager instanceof Manager) {
			this.manager = manager;
			return true;
		} else {
			return false;
		}
	}

	@Override
	/**
	 * Methode toString geeft een string representatie terug van de
	 * eigenschappen van het object.
	 *
	 * @return string representatie van het object
	 */
	public abstract String toString();
}
