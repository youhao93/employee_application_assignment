package domein;

/**
 * Subklasse Manager van de superklasse Employee. Het implementeert de abstracte methoden
 * van de superklasse Employee en heeft ookeigen methodes.
 */
public class Manager extends Employee {


    private double bonus = 0.0;

    /**
     * Maakt een nieuw Manager-object aan met id nummer, maandsalaris en bonus.
     *
     * @param id id nummer van de manager
     * @param monthlySalary maandsalaris van de manager
     * @param bonus bonus van de manager
     */
    public Manager(int id, double monthlySalary, double bonus) {
        super(id, monthlySalary);
        this.bonus = bonus;
    }

    @Override
    /**
     * Methode payAmount geeft het totale maandsalaris terug.
     *
     * @return totale maandsalaris
     */
    public double payAmount() {
        return super.payAmount() + bonus;
    }


    @Override
    /**
     * Methode toString geeft een string representatie terug van de
     * eigenschappen van dit Manager-object.
     *
     * @return string representatie van het Manager-object
     */
    public String toString(){
        String s = 	"Type: Manager, id: " + getId() + ", monthly salary: "
                + getMonthlySalary() + ", bonus: " + bonus;
        if (getManager() !=null) {
            s = s + ", " + "manager: " + getManager().getId();
        }
        s = s + ".\n";
        return s;
    }
}
