package domein;

/**
 * Subklasse Salesman van de superklasse Employee. Het implementeert de abstracte methoden
 * van de superklasse Employee en heeft ookeigen methodes.
 */
public class Salesman extends Employee {

    private double commission = 0.0;

    /**
     * Maakt een nieuw Salesman-object aan met id nummer, maandsalaris en commissie.
     *
     * @param id id nummer van de Salesman
     * @param monthlySalary maandsalaris van de Salesman
     * @param commission commissie van de Salesman
     */
    public Salesman(int id, double monthlySalary, double commission) {
        super(id, monthlySalary);
        this.commission = commission;
    }

    @Override
    /**
     * Methode payAmount geeft het totale maandsalaris terug.
     *
     * @return totale maandsalaris
     */
    public double payAmount() {
        return super.payAmount() + commission;
    }


    @Override
    /**
     * Methode toString geeft een string representatie terug van de
     * eigenschappen van dit Salesman-object.
     *
     * @return string representatie van het object
     */
    public String toString(){
        String s = 	"Type: Salesman, id: " + getId() + ", monthly salary: "
                + getMonthlySalary() + ", commission: " + commission;
        if (getManager() !=null) {
            s = s + ", " + "manager: " + getManager().getId();
        }
        s = s + ".\n";
        return s;
    }
}
