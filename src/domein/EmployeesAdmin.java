package domein;

import java.util.ArrayList;

/**
 * Klasse EmployeesAdmin is waar alle Employee objecten in bewaard worden.
 *
 * In deze klasse kunnen objecten van de subklassen van Employee toegevoegd worden.
 * Daarbij wordt er geheckt of er geen objecten zijn die al in de lijst voorkomen.
 * Er kan een berekening worden gemaakt om alle salarisbetalingen bij elkaar op te tellen.
 * Het kan een String uitprinten met de bijbehorendegegevens van de toegevoegde
 * Employees. Het hoogste id getal in de lijst kan opgehaald worden en als laatst
 * kan met behulp van index nummers een subklasse van Employee een Manager krijgen.
 */

public class EmployeesAdmin {

	private ArrayList<Employee> employees = null;

	/**
	 * Maakt een nieuwe Employee lijst aan voor Employee objecten.
	 */
	public EmployeesAdmin() {
		employees = new ArrayList<Employee>();
	}

	/**
	 * Methode addEngineer controleert alvorens het toevoegen
	 * of er een object met zelfde id in de lijst bevindt. Zo
	 * ja, dan wordt het nieuwe Engineer-object niet toegevoegd en geeft
	 * het een false terug. Als er geen object met zelfde id
	 * in de lijst bevindt, dan geeft het een true terug waarbij
	 * het nieuwe object aan de lijst is toegevoegd.
	 * @param id id van Engineer-object
	 * @param salary salaris van Engineer-object
	 * @return een true als het Engineer-object is toegevoegd, en anders
	 * een false
	 *
	 */
	public boolean addEngineer(int id, double salary) {
		boolean res = true;
		Engineer e = new Engineer(id, salary);
		if(employees.contains(e)) {
			res = false;
		} else {
			employees.add(e);
		}
		return res;
	}

	/**
	 * Methode addSalesman controleert alvorens het toevoegen
	 * of er een object met zelfde id in de lijst bevindt. Zo
	 * ja, dan wordt het nieuwe Salesman-object niet toegevoegd en geeft
	 * het een false terug. Als er geen object met zelfde id
	 * in de lijst bevindt, dan geeft het een true terug waarbij
	 * het nieuwe object aan de lijst is toegevoegd.
	 * @param id id van Salesman-object
	 * @param salary salaris van Salesman-object
	 * @param commissie commissie van Salesman object
	 * @return een true als het Salesman-object is toegevoegd, en anders
	 * een false
	 */
	public boolean addSalesman(int id, double salary, double commissie) {
		boolean res = true;
		Salesman s = new Salesman(id, salary, commissie);
		if(employees.contains(s)) {
			res = false;
		} else {
			employees.add(s);
		}
		return res;
	}

	/**
	 * Methode addManager controleert alvorens het toevoegen
	 * of er een object met zelfde id in de lijst bevindt. Zo
	 * ja, dan wordt het nieuwe Manager-object niet toegevoegd en geeft
	 * het een false terug. Als er geen object met zelfde id
	 * in de lijst bevindt, dan geeft het een true terug waarbij
	 * het nieuwe object aan de lijst is toegevoegd.
	 * @param id id van Manager-object
	 * @param salary salaris van Manager-object
	 * @param bonus bonus van Manager object
	 * @return een true als het Manager-object is toegevoegd, en anders
	 * een false
	 */
	public boolean addManager(int id, double salary, double bonus) {
		boolean res = true;
		Manager m = new Manager(id, salary, bonus);
		if(employees.contains(m)) {
			res = false;
		} else {
			employees.add(m);
		}
		return res;
	}

	/**
	 * Methode calcTotalPayAmount gaat alle Employee objecten
	 * af waarvan het salaris wordt opgehaald en alles
	 * bij elkaar optelt. Indien het een Salesman betreft
	 * wordt de commissie erbij opgeteld en als het een Manager
	 * betreft wordt de bonus erbij opgeteld.
	 *
	 * @return totale salarisbetaling
	 */
	public double calcTotalPayAmount() {
		double res = 0.0;
		for (Employee e : employees) {
			res = res + e.payAmount();
		}
		return res;
	}

	@Override
	/**
	 * Geeft een string representatie terug die de eigenschappen toont
	 * van alle Employee subklasse objecten, waarvan elke omschrijving van
	 * een object op een aparte regel staat.
	 *
	 * @return string representatie van alle subklassen van de Employee-objecten
	 */
	public String toString() {
		String res = "";
		for (Employee e : employees) {
			res = res + e.toString();
		}
		return res;
	}

	/**
	 * Methode getHightestId geeft het hoogste id van een Employee
	 * object terug in de lijst.
	 *
	 * @return het grootste id getal van een Employee object
	 */
	public int getHighestId() {
		int res = -1;
		for (Employee e : employees) {
			if (e.getId() > res) {
				res = e.getId();
			}
		}
		return res;
	}

	/**
	 * Methode setManager zorgt ervoor dat een employee met de ingegeven
	 * index nummer een Manager krijgt uit de lijst krijgt toegewezen met het
	 * gegeven index nummer.
	 *
	 * @param employee het Employee-object dat een manager krijgt
	 * @param manager de Manager die aan het Employee-object wordt toegewezen
	 *
	 * @return true als de manager aan het Employee-object is toegewezen,
	 * anders een false.
	 */
	public boolean setManager(int employee,  int manager) {
		return employees.get(employee).setManager(employees.get(manager));
	}
}
