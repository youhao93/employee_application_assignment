package domein;

/**
 * Een voorbeeldtoepassing van het Employee programma
 */
public class EmployeesAdminMain {

	private EmployeesAdmin es = null;
	
	public void init() {
	   es = new EmployeesAdmin();

	   es.addEngineer(1, 1000);
	   /*
	   Het onderstaande Engineer object wordt dus niet toegevoegd, met salaris
	   1234. Zie ook de standaarduitvoer.
	    */
	   es.addEngineer(1, 1234);
	   es.addEngineer(2, 1000);
	   es.addSalesman(3, 1000, 100);
	   es.addManager(4, 2000, 1000);

	   es.setManager(0,3);
	   es.setManager(1,3);
	   es.setManager(2,3);
	   es.setManager(3,3);
	}
	
	public void testEmployees() {	                           
       System.out.println(es.toString());
	}
	
	public static void main(String[] args) {
	 EmployeesAdminMain te = new EmployeesAdminMain();	
	 te.init();
	 te.testEmployees();
	}

}
