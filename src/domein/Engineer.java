package domein;

/**
 * Subklasse Engineer van de superklasse Employee. Het implementeert de abstracte methoden
 * van de superklasse Employee en heeft ookeigen methodes.
 */
public class Engineer extends Employee{



    /**
     * Maakt een nieuw Engineer-object aan met id nummer en maandsalaris.
     *
     * @param id id nummer van de Engineer
     * @param monthlySalary maandsalaris van de Engineer
     */
    public Engineer(int id, double monthlySalary) {
        super(id, monthlySalary);
    }


    @Override
    /**
     * Methode payAmount geeft het totale maandsalaris terug.
     *
     * @return totale maandsalaris
     */
    public double payAmount() {
        return super.payAmount();
    }

    @Override
    /**
     * Methode toString geeft een string representatie terug van de
     * eigenschappen van dit Engineer object.
     *
     * @return string representatie van het object
     */
    public String toString(){
        String s = 	"Type: Engineer, id: " + getId() + ", monthly salary: "
                + getMonthlySalary();
        if (getManager() !=null) {
            s = s + ", " + "manager: " + getManager().getId();
        }
        s = s + ".\n";
        return s;
    }
}



